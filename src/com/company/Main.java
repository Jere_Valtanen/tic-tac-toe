package com.company;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static int input = 0;
    public static String[] board = new String[9];
    public static Boolean GameOver = false;
    public static int turn = 0;

    public static void main(String[] args) throws IOException {

        InitBoard();

        String inputText = "";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (!GameOver) {
            PrintBoard();

            while (!tryParseInt(br.readLine())) {
                System.out.println("Try again");
            }
            switch (turn) {
                case 0: {
                    board[input - 1] = "X";
                    turn ++;
                    break;
                }
                case 1: {
                    board[input - 1] = "O";
                    turn --;
                    break;
                }

            }

            WinCheck();
        }
    }

    static boolean tryParseInt(String value) {
        try{
            input = Integer.parseInt(value);
            if(input > 0 && input < 10 && (board[input - 1].matches(Integer.toString(input))))
                return true;
            else
                return false;
        } catch (NumberFormatException e)
        {
            return false;
        }
    }

    static void InitBoard()
    {
        for(int i = 0; i < 9; i++)
        {
            board[i] = String.valueOf((i + 1));
        }
    }

    static void PrintBoard()
    {
        int value = 0;
        for(int i = 0; i < 3; i++)
        {
            System.out.print("|");
            for(int u = 0; u < 3; u++)
            {
                System.out.print(board[value] + " | ");
                value ++;
            }
            System.out.println("\n---+---+---");
        }
    }

    static void WinCheck()
    {
        String player = "";
        for (int i = 0; i < 2; i ++)
        {
            switch (i) {
                case 0:
                    player = "X";
                    break;
                case 1:
                    player = "O";
                    break;
            }

            if (
                    (board[0] == player && board[1] == player && board[2] == player) ||
                            (board[3] == player && board[4] == player && board[5] == player) ||
                            (board[6] == player && board[7] == player && board[8] == player) ||
                            (board[0] == player && board[3] == player && board[6] == player) ||
                            (board[1] == player && board[4] == player && board[7] == player) ||
                            (board[2] == player && board[5] == player && board[8] == player) ||
                            (board[0] == player && board[4] == player && board[8] == player) ||
                            (board[2] == player && board[4] == player && board[6] == player)) {
                GameOver = true;
                PrintBoard();
                System.out.println(player + " WINS");
            }
        }
    }
}
